// fseek() function is use read the characters from start,end,or a current from  the file..
//
// fseek() has three parameters...  fseek( fp, how many char wants to read, constant(0,1,2))
//
#include<stdio.h>
void main(){
	 FILE *fp=fopen("Demo.txt","r");
	
       //	 fseek(fp,7,0) ;// it gives characters from the file from starting excepting the we give the constant number....
 	rewind(fp);
          fseek(fp,7,1);// it gives the characters from the current position...
	// fseek(fp,-7,2);// it gives characters from the last of the filem...

	char ch;

	 while((ch=fgetc(fp))!=EOF){

		 printf("%c",ch);

	 }
	 
	}

