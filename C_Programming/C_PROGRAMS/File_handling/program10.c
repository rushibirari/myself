
// use of functions fgetc()   &   fputc()
//
// fgetc()  :   this function is use to read charater by character from a file..
//
// Program for read character by character from file..
//
  #include<stdio.h>
void main(){

	FILE *fp=fopen("Demo.txt","r");
	char ch;

	while(ch=fgetc(fp)!=EOF){

		printf("%c",ch);
	}

	printf("%ld\n",(ftell(fp)-1));
}


