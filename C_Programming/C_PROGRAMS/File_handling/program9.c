

// use of getw() and putw()
//
#include<stdio.h>
void main(){

	FILE *fp =fopen("Demo.txt","w+");
	int num1=10;
	int num2=20;
	int num3;
	char ch='A';
	int *ptr;

	printf("%ld\n",ftell(fp));

	putw(num1,fp);
	putw(num2,fp);
	putw(num3,fp);
	putw(ch,fp);
//	putw(ptr,fp);

	printf("%ld\n",ftell(fp));

	rewind(fp);
	printf("%d\n",getw(fp));
	printf("%d\n",getw(fp));

}


