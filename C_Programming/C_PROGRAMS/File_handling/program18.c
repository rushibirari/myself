//
//  Function like macros....
//
  #include<stdio.h>

 #define add(a,b) a+b     // In this function like macros we can define a function which has a short work..
			  // and we can pass the parameter and gives a function work..

 #define sqr(x) x*x


void main(){

	int x=10,y=20;

	printf("%d\n",add(x,y));

	printf("%d\n",sqr(x));

	}


