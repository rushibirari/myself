//
//  Use of ftell() function in file handling 
//  it is used to telling the user that at which point the pointer or ftell is stop 
//  or the count of characters in the file.....
//
//
#include<stdio.h>
void main(){


	FILE 	*fp=fopen("Newone.txt","w");

	printf("%ld\n",ftell(fp));

	fprintf(fp,"hello world...");

	printf("%ld\n",ftell(fp));

}

