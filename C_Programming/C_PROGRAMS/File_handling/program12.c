
// Program to copy characters until certain limit..
//
//
#include<stdio.h>
void main(){

	FILE *fp1=fopen("Demo.txt","r");
	FILE *fp2=fopen("Success.txt","w");

	int count=0;
	char ch;

	while((ch=fgetc(fp1))!=EOF  && ch!=5){

		fputc(ch,fp2);
		count++;
	}

}
