//
//use of fclose()
//
#include<stdio.h>
void main(){

	FILE *fp=fopen("Demo.txt","r");

	printf("%c\n",fgetc(fp));
	printf("%c\n",fgetc(fp));

	fclose(fp);

	printf("%c\n",fgetc(fp));// after file close it gives a random character ..

}
