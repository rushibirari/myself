
// preprocessor directives..
//
// the things which are start from # then it is called as preprocessor.
//
//it is also known as macros and it has two types 1) object like macros 2) function like macros...

#include<stdio.h>

#define xyz 10

#define rahul char
void main(){

	 printf("%d\n",xyz); // xyz is replaced by 10 at the time of preprocessing...
			     // the things which are replaced at preprocessing time called as macros...

	 rahul ch ='a';
	 printf("%c\n",ch);
}

