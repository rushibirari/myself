
// WAP to reverse the array in a given range by user...
//

#include<stdio.h>

int reverse_range(int *arr, int size, int start, int end){

	int temp;

	for(int i = start,j=0; i<(start+end)/2 + 1;i++,j++){

		temp = arr[i];
		arr[i] = arr[end-j];
		arr[end-j] = temp;

	}

	printf("\nAfter reversed array :\n");

	for(int i= 0;i<size;i++){
		printf("%d\t",arr[i]);
	}

	printf("\n\n");
	return 0;


}


void main(){

	int size,start,end;
	printf("\nEnter the size of array  : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array  :  ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("\nEnter the range start & end :  ");;
	scanf("%d %d",&start,&end);

	reverse_range(arr,size,start,end);


}
