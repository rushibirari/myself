
// WAP to reverse the whole array..
//

#include<stdio.h>

int reverse_array(int *arr, int size){

	int new_arr[size];

	for(int i=size-1,j=0;i>=0;i--,j++){

		new_arr[j] = arr[i];

	}

	

	printf("\nAfter reversed array :\n");

	for(int i= 0;i<size;i++){
		printf("%d\t",new_arr[i]);
	}

	printf("\n\n");
	return 0;


}


void main(){

	int size,start,end;
	printf("\nEnter the size of array  : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array  :  ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}


	reverse_array(arr,size);


}
