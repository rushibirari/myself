

// WAP to find the count of the greatest number of elements from array...
//
#include<stdio.h>

void main(){

	int size;
	printf("\nEnter gthe size of array  :  ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i=0; i< size;i++){
		scanf("%d",&arr[i]);
	}

	printf("\nArray elements are : ");
	for(int i=0;i<size;i++){
		printf("%d   ",arr[i]);
	}
	// optimize approach
	int count=0;

	int max= arr[0];

	for(int i =1; i<size;i++){
		if(arr[i] > max){
			max= arr[i];
		}
	}

	for(int i=0;i< size;i++){

		if(max == arr[i]){
			count++;
		}

	}

	count = size - count;

	printf("\ncount is  = %d   \n\n",count);
}

