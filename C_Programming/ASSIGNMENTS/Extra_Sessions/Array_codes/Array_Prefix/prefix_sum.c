

// WAP to find the sum of array by using prefix array...
//
// within the given range
//
// In this problem there is a array is given of n numbers and also given number of queries which they want to fire(operations)
//
//  i/p : arr[ 1,2,3,6,4,5,89,8,55,5,5,77];
//
//  no. of queries = 5
//
#include<stdio.h>

void main(){

	int size;
	printf("\nEnter the array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : \n");
	for(int i = 0; i<size;i++){
		scanf("%d",&arr[i]);
	}

	// prefix array sum
	int pre_Arr[size];
	pre_Arr[0]= arr[0];
	for(int i = 1; i < size ; i++){

		pre_Arr[i] = pre_Arr[i-1] + arr[i];

	}

	printf("\n Prefix sum array is : \n");
	for(int i =0 ;i<size;i++){
		printf("%d  ",pre_Arr[i]);
	}

	printf("\n\n");	
}


