

// WAP to find the the given array is equilibrium or not....
//
//
#include<stdio.h>

int  main(){

	int size;
	printf("\nEnter the size of array  :   ");
	scanf("%d",&size);
	int arr[size];

	printf("Enter the elements of array  : ");
	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}

	int lower_sum, right_sum;

	for(int i = 0; i< size; i++){

		lower_sum = 0;

		for( int j = 0; j < i; j++){

			lower_sum = lower_sum + arr[j];
		}

		right_sum =0;

		for(int j = i+1; j< size; j++){

			right_sum = right_sum + arr[j];
		}
		if(lower_sum == right_sum){
			
			printf("\n\nindex =  %d   \n",i);
			break;
		}

	}

	return -1;

}
