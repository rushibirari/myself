

// wap for inplace prefix array sum...
//
//
#include<stdio.h>

void main(){


	int size;
	printf("\nEnter the size of array : \n");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array :\n ");

	for(int i =0;i<size;i++){
		scanf("%d",&arr[i]);

	}

	printf("\nArray before  prefix  :\n");
	for(int i = 0;i<size;i++){
		printf("%d   ",arr[i]);
	}

	for(int i = 1;i<size;i++){

		int sum = arr[i] + arr[i-1];

		arr[i] = sum;
	}

	printf("\nArray after  prefix  :\n");
	for(int i = 0;i<size;i++){
		printf("%d   ",arr[i]);
	}
	printf("\n\n");
}
