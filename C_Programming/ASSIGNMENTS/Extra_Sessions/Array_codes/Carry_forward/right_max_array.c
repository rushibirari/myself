

// WAP to find the right max element and return the whole array..
//
//Given N number of array and find the right max element and return new array::
//
//i/p : Arr = [4,2,3,6,87,8,1,99];
//
//o/p : left_max[] = [99,99,99,99,99,99,99];
//
#include<stdio.h>
void main(){

	int size;
	printf("\nEnter the size of array  :  ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("\nArray is :  ");
	for(int i=0;i< size;i++){
		printf("%d   ",arr[i]);
	}

	// for finding the right max element;
	// bruteforce approach...
	
	int right_max[size];

	for(int i=size-1;i>=0;i--){
		int max_ele = arr[i];

		for(int j=size-1; j>= i;j--){

			if(arr[j] > max_ele){

				max_ele = arr[j];
			}
		}

		right_max[i] = max_ele;
	}

	printf("\n\nNew array is : ");
	for(int i=0;i< size;i++){
		printf("%d   ",right_max[i]);
	}
	printf("\n\n");

}

