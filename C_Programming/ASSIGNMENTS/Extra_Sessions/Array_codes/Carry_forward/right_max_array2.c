

// WAP to find the right max element and return the whole array..
//
//Given N number of array and find the right max element and return new array::
//
//i/p : Arr = [4,2,3,6,87,8,1,99];
//
//o/p : left_max[] = [99,99,99,99,99,99,99,99];
//
#include<stdio.h>
void main(){

	int size;
	printf("\nEnter the size of array  :  ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("\nArray is :  ");
	for(int i=0;i< size;i++){
		printf("%d   ",arr[i]);
	}

	// for finding the left max element;
	// optimize approach...
	
	int right_max[size];

	right_max[size-1]= arr[size-1];

	for(int i = size-2; i>=0;i--){

		if(right_max[i+1] < arr[i]){

			right_max[i] = arr[i];
		}else{
			right_max[i]= right_max[i+1];
		}
	}

	printf("\n\nNew array is : ");
	for(int i=0;i< size;i++){
		printf("%d   ",right_max[i]);
	}
	printf("\n\n");

}

