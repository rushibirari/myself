

// WAP to find the left max element and return the whole array..
//
//Given N number of array and find the left max element and return new array::
//
//i/p : Arr = [4,2,3,6,87,8,1,99];
//
//o/p : left_max[] = [4,4,4,6,87,87,87,99];
//
#include<stdio.h>
void main(){

	int size;
	printf("\nEnter the size of array  :  ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("\nArray is :  ");
	for(int i=0;i< size;i++){
		printf("%d   ",arr[i]);
	}

	// for finding the left max element;
	// optimize approach...
	
	int left_max[size];

	left_max[0]= arr[0];

	for(int i=1 ;i<size;i++){

		if(left_max[i-1] < arr[i]){

			left_max[i] = arr[i];
		}else{
			left_max[i]= left_max[i-1];
		}
	}

	printf("\n\nNew array is : ");
	for(int i=0;i< size;i++){
		printf("%d   ",left_max[i]);
	}
	printf("\n\n");

}

