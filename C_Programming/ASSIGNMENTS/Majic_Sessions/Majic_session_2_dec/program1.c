//
//
//WAP to create the node given by user and get data and print all nodes..
//
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Mall{

	char m_name[20];
	int n_shops;
	float m_revenue;

	struct Mall *next;

}mall;

  mall *head=NULL;// global declaration..

  void addnode(){

	  mall *newnode=(mall *)malloc(sizeof(mall));

	  getchar();
	  printf("Enter the Mall Name  : ");
	  fgets(newnode->m_name,20,stdin);

	  printf("Enter the no. of shops  :  ");
	  scanf("%d",&newnode->n_shops);

	  printf("Enter the mall revenue  : ");
	  scanf("%f",&newnode->m_revenue);

	  newnode->next=NULL;

	  //getchar();
	
	  if(head==NULL){
		  head=newnode;
	  }
	  else{
		  mall *temp=head;

		  while(temp->next!= NULL){

			  temp=temp->next;
			 }
		  temp->next=newnode;

	  }

       }


  void printdata(){

	  mall *temp=head;

	  printf("\n The linked list is  :  \n");
	  while(temp!=NULL){
		  temp->m_name[strcspn(temp->m_name,"\n")]='\0';
		 

		  printf("| %s =  %d  =  %.4f  |-->",temp->m_name,temp->n_shops,temp->m_revenue);

		  temp=temp->next;

	  }
	  printf("NULL");

	 }


     void main(){

	     int n ;

	     printf("Enter the number of nodes u want add :  ");
	     scanf("%d",&n);

	     for(int i=1;i<=n;i++){
		     addnode();
	     }

	     printdata();

	     printf("\n\n");

     }
