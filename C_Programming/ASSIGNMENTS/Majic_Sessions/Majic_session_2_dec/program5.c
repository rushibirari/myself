

//  WAP to print the linked list data given by user..
//
// And find the total of data given by user..

#include<stdio.h>
#include<stdlib.h>

typedef struct IntData{

	int data;

	struct IntData *next;

}data;

  data *head=NULL;

  void addnode(){
        data *newnode=(data *)malloc(sizeof(data));

	printf("Enter the number   :");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	if(head==NULL){
		head=newnode;
	}
	else{
		data *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;

	}

  }

 void printdata(){

	 data *temp=head;

	 int sum=0;// to store the sum of all integer data in linkedlist

	 if(temp==NULL){
		 printf("No node exits..\n");
	 }else{
		 while(temp!=NULL){
			 printf("| %d |-->",temp->data);

			 sum =sum + temp->data;

			 temp=temp->next;
		 }

		 printf("NULL\n");
		 printf("\nTotal sum is  : %d  \n",sum);

	 }
 }

 void main(){

	 int n;
	 printf("How many nodes u want to enter  : ");
	 scanf("%d",&n);

	 for(int i=1;i<=n;i++){

		 addnode();
	 }

	 printdata();

	 printf("\n\n");

 }

