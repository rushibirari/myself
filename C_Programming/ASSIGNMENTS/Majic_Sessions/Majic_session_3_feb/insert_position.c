
// WAP to find the position for the given key by the user..
//
// here array is sorted... and return index for the user..
//
#include<stdio.h>

int Insert_position(int *arr, int size,int key){
	
	int start =0,end=size-1,mid;

	while(start<=end){

		mid= (start+end)/2;

		if(key < arr[0])
			return -1;
		if(key > arr[size-1])
			return -1;
		if(arr[mid] == key)
			return mid;
		if(arr[mid] > key){

			end = mid-1;

		}

		if(arr[mid] < key){

			start = mid+1;
		}		

	}

}
void main(){

	int size;
	printf("\nEnter the size of array : ");
	scanf("%d",&size);

	int arr[size];
	printf("\nEnter the elements of array in the form of sorted :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int key;

	printf("\nEnter the element you want to insert at position  :");
	scanf("%d",&key);

	int retval = Insert_position(arr,size,key);

	if(retval == -1)
		printf("%d is not insert at any position \n",key);
	else
		printf("%d is insert at %d position \n\n",key,retval);

}
