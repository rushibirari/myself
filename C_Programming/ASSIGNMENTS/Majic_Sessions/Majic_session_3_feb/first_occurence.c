
// WAP to find the first occurence of distinct element in an array...
//
  
#include<stdio.h>

int first_occurence(int *arr,int size,int key){

	int start= 0,end=size-1;
	int mid,store=-1;

	while(start <=end){

		mid=(start + end)/2;

		if( key > arr[size-1]){

			return store;
		}
		if( key < arr[0]){
			return store;
		}

		if(arr[mid] == key){

			store =mid;
			return store;
		}
		if(arr[mid]> key){
			end= mid-1;
		}
		if(arr[mid]< key){
			start =mid+1;
		}
		
	}

	return store;

}
void main(){


	int size;

	printf("\nEnter the array size:  ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the array elements : ");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("\nEnter the key : ");
	scanf("%d",&key);

	int retval = first_occurence(arr,size,key);

	if(retval == -1){
		printf("\nElement is not present...\n");
	}
	else{
		printf("\nElement is present at %d index  \n",retval);
	}
}
