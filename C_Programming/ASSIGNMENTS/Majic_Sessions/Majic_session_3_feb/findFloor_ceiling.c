
// Wap program to find the floor number of a given element..


#include<stdio.h>

int FloorValue(int *arr , int size, int key){

	int mid, store = -1,start = 0, end = size-1;

	while(start<=end){

		mid = (start + end)/2;

		if(key > arr[size-1])
			return arr[size-1];

		if(key < arr[0])
			return -1;

		if(arr[mid] == key){
		
			return arr[mid];
		}
		if(arr[mid] > key){

			end= mid-1;

		}
		if(arr[mid] < key){
			store= arr[mid];
			start = mid +1;

		}

	}

	return store;


}


int CeilingValue(int *arr , int size, int key){

	int mid, store = -1,start = 0, end = size-1;

	while(start<=end){

		mid = (start + end)/2;

		if(key > arr[size-1])
			return arr[size-1];

		if(key < arr[0])
			return -1;

		if(arr[mid] == key){
		
			return arr[mid];
		}
		if(arr[mid] > key){

			store = arr[mid];
			end= mid-1;

		}
		if(arr[mid] < key){
		
			start = mid +1;

		}

	}
	return store;

}

void main(){

	int key,size;


	printf("\nEnter the size of array : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i = 0; i < size;i++){
 
		scanf("%d",&arr[i]);

	}

	printf("\nEnter the key :");
	scanf("%d",&key);

	int retval = FloorValue(arr,size, key);
	int retval1 = CeilingValue(arr,size,key);


	if(retval == -1 && retval1== -1){

		printf("\nFloor & ceiling value is not present...\n");
	}
	else{
		printf("\nFloor value is :  %d  &  Ceiling value is %d  \n ",retval,retval1);

	}
	

}
