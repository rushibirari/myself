

//WAP to find the element in a rotated array ..
//Binary search 

#include<stdio.h>

int rotate_array(int *arr, int size, int key,int peak){


	int start=0,end=size-1,mid,store=-1;

		if(key >= arr[0] && key <= arr[peak]){
			start=0;
			end=peak;
		}else{
			start = peak + 1;
		}

	while(start<=end){


	//	mid= (start + (end-start))/2;

		mid = (start + end)/2;

		if(arr[mid] == key){
			store= mid;
			return store;
		}

		if(arr[mid]> key){

			end=mid-1;
		}
		if(arr[mid]< key){
			start= mid+1;
		}

	}
	return store;


}


void main(){


	int size;
	printf("\nEnter the array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the array elements in rotated form  : \n ");
	for(int i=0;i<size;i++)
		scanf("%d",&arr[i]);

	int peak,key;

	printf("\nEnter the key : ");
	scanf("%d",&key);

	for(int i=0;i<size-1;i++){

		if(arr[i]>arr[i+1]){
			peak=i;
		//	break;
		}
	}

	int retvalue= rotate_array(arr, size, key,peak);

	if(retvalue == -1){

		printf("\nElement is not present \n");
	}
	else{

		printf("\nElement is present at %d index\n\n",retvalue);

	}


	}


