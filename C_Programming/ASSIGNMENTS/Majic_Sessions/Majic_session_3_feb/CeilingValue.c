
// Wap program to find the floor number of a given element..


#include<stdio.h>

int FloorValue(int *arr , int size, int key){

	int mid, store = -1,start = 0, end = size-1;

	while(start<=end){

		mid = (start + end)/2;

		if(key > arr[size-1])
			return -1;

		if(key < arr[0])
			return arr[0];

		if(arr[mid] == key){
		
			return arr[mid];
		}
		if(arr[mid] > key){

			store = arr[mid];
			end= mid-1;

		}
		if(arr[mid] < key){

			start = mid +1;

		}

	}

	return store;


}
void main(){

	int key,size;


	printf("\nEnter the size of array : ");
	scanf("%d",&size);

	int arr[size];

	printf("\nEnter the elements of array : ");
	for(int i = 0; i < size;i++){
 
		scanf("%d",&arr[i]);

	}

	printf("\nEnter the key :");
	scanf("%d",&key);

	int retval = FloorValue(arr,size, key);

	if(retval == -1){

		printf("\nCeiling is not present...\n");
	}
	else{
		printf("\nCeiling value is :  %d \n ",retval);

	}
	

}
