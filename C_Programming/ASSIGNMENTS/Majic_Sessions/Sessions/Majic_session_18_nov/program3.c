//
//
//WAP to store the data on heap section using 2D array  ..
//
#include<stdio.h>
#include<stdlib.h>

void main(){

	int rows,cols,sum=0;
	printf("Enter the rows & cols respectively : \n");
	scanf("%d%d",&rows,&cols);

	int *marks =(int *) malloc(rows*cols * sizeof(int));
	
	printf("Enter the marks  : \n");

	for(int i=0;i<rows*cols;i++){

		scanf("%d",&marks[i]);
		sum=sum+marks[i];
	}

	printf("Entered marks are subject wise  : \n");

	for(int i=0;i<rows*cols;i++){
		printf("Sub%d = %d\n",i,(marks[i]));
	}
     int num=rows*cols;

        printf("Total of %d subjects = %d \n\n",num,sum);

}
