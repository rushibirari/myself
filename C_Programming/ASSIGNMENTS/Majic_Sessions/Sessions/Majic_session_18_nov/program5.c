
//WAP to to free the space from haep sections...
//
#include<stdio.h>
#include<stdlib.h>
void main(){

	int rows=4;
	int col=3;


  int **ptr= (int**)malloc(rows * sizeof(int*));// creating the space on heap section...using double pointer...

  	for(int i=0;i<rows;i++){
		ptr[i]=(int*) malloc(col * sizeof(int));
	}

	for(int i=0;i<rows;i++){
		free(ptr[i]);// free the space of array in heap section of ptr data

	}

	free(ptr);// free the space in heap section..
}
