//
//
//WAP to store the data on heap section using 3D array  ..
//
#include<stdio.h>
#include<stdlib.h>

void main(){

	int rows,cols,plane,sum=0;
	printf("Enter the rows & cols & plane respectively : \n");
	scanf("%d%d%d",&rows,&cols,&plane);

	int *marks =(int *) malloc(rows*cols*plane  * sizeof(int)); // to get the space on haep section for 3d Array..
	
	printf("Enter the marks  : \n");

	for(int i=0;i<rows*cols*plane ;i++){

		scanf("%d",&marks[i]);
		sum=sum+marks[i];
	}

	printf("Entered marks are subject wise  : \n");

	for(int i=0;i<rows*cols*plane ;i++){
		printf("Sub%d = %ld\n",i,*(marks+i));// it is also used as marks[i]  instead of *(marks+i)
	}
     int num=rows*cols*plane;

        printf("Total of %d subjects = %d \n\n",num,sum);

	printf("%ld\n",sizeof(marks));
	free(marks);
}
