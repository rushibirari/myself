
// WAP to DMA (Dynamic Memory Allocation)
//
#include<stdio.h>
#include<stdlib.h>
void main(){

	int x,sum=0;
	printf("Enter the no. of subjects : \n");
	scanf("%d",&x);

	int *marks = (int *)malloc( x * sizeof(int));
	
	printf("Enter the marks  :\n");
	for(int i=0;i<x;i++){

		scanf("%d",&marks[i]);
		sum=sum + marks[i];
	}
	printf("\n\n");
	for(int i=0;i<x;i++){

		printf("sub%d  =  %d\n",i,(marks[i]));
	}
	int i=x;
	printf("Total marks of %d subject are  :  %d  \n\n",i,sum);
         
	free(marks);
}

