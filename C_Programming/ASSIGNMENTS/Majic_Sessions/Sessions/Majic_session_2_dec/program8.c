

//  WAP to print the linked list data given by user..
//
// And find the minimum number among data given by user..

#include<stdio.h>
#include<stdlib.h>

typedef struct IntData{

	int data;

	struct IntData *next;

}data;

  data *head=NULL;

  void addnode(){
        data *newnode=(data *)malloc(sizeof(data));

	printf("Enter the number   :");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	if(head==NULL){
		head=newnode;
	}
	else{
		data *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;

	}

  }

 void printdata(){

	 data *temp=head;
	 int min = 0;// storing minimum no.


	 if(temp==NULL){
		 printf("No node exits..\n");
	 }else{
		 while(temp!= NULL){
			
		
			 printf("| %d |-->",temp->data);
	 			if(min > temp->data){
					min =temp->data;
				}			

			 temp=temp->next;
		
            }

		 printf("NULL\n");
		 printf("\nMinimum number is   : %d  \n",min);

	 }
 }

 void main(){

	 int n;
	 printf("How many nodes u want to enter  : ");
	 scanf("%d",&n);

	 for(int i=1;i<=n;i++){

		 addnode();
	 }

	 printdata();

	 printf("\n\n");

 }

