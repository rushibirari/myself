//
//
//WAP to create the node given by user and get data and print all nodes..
//information of festivals in maharashtra..

//
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct festival{

	char fes_name[20];
	int fes_people;
	float fes_budget;

	struct festival *next;

}fes;

  fes *head=NULL;// global declaration..

  void addnode(){

	  fes  *newnode=(fes *)malloc(sizeof(fes));

	  getchar();
	  printf("Enter the festival Name       : ");
	  fgets(newnode->fes_name,20,stdin);

	  printf("Enter the no. people          :  ");
	  scanf("%d",&newnode->fes_people);

	  printf("Enter the festival budget     : ");
	  scanf("%f",&newnode->fes_budget);

	  newnode->next=NULL;
	
	  if(head==NULL){
		  head=newnode;
	  }
	  else{
		  fes *temp=head;

		  while(temp->next!= NULL){

			  temp=temp->next;
			 }
		  temp->next=newnode;

	  }

       }


  void printdata(){

	  fes *temp=head;
		int cnt=0;
	  printf("\n The linked list is  :  \n");
	  while(temp!=NULL){
		  temp->fes_name[strcspn(temp->fes_name,"\n")]='\0';
		 

		  printf("| %s =  %d  =  %.4f  |-->",temp->fes_name,temp->fes_people,temp->fes_budget);
			cnt++;
		  temp=temp->next;

	  }
	  printf("NULL\n");
	  printf("\nTotal No. of nodes is  :  %d  \n",cnt);
	 }


     void main(){

	     int n ;

	     printf("Enter the number of nodes u want add :  ");
	     scanf("%d",&n);

	     for(int i=1;i<=n;i++){
		     addnode();
	     }

	     printdata();

	     printf("\n\n");

     }
