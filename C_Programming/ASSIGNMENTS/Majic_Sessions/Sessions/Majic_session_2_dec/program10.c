

// Create a structure of realtime example of linked list
//
  #include<stdio.h>
  #include<stdlib.h>
  #include<string.h>
  typedef struct petrol_pump{

	  char p_name[20];
	  int no_p;
	  int tot_veh;
	  float petrol_p;
	  float diesel_p;

	  struct petrol_pump *next;
  }pp;
    
	pp *head=NULL;

	void addnode(){

		pp *newnode=(pp *)malloc(sizeof(pp));
		getchar();

		printf("\nEnter the name of petrol pump     :  ");
		fgets(newnode->p_name,20,stdin);
		printf("\nEnter the number of petrolpumps   :  ");
		scanf("%d",&newnode->no_p);
		printf("\nEnter the total vehicles arrived  :  ");
		scanf("%d",&newnode->tot_veh);
		printf("\nEnter the petrol price            :  ");
		scanf("%f",&newnode->petrol_p);
		printf("\nEnte the diesel price            :  ");
		scanf("%f",&newnode->diesel_p);

		
		newnode->next=NULL;

		if(head==NULL){
			head=newnode;
		}
		else{
			pp *temp=head;

			while(temp->next!=NULL){
				temp=temp->next;
			}
			temp->next=newnode;

		}
		printf("\n");
	}


    void printdata(){

	    pp *temp=head;

	    while(temp!=NULL){

		    if(temp==NULL)
			    printf("\n No node exist...");
		    else{
				temp->p_name[strcspn(temp->p_name,"\n")]= '\0' ;

				printf("| %s  =  %d  =  %d  =  %.3f  =  %.3f  |--->",temp->p_name,temp->no_p,temp->tot_veh,temp->petrol_p,temp->diesel_p);

		    	temp=temp->next;
		    }	
	    
	    }
	    printf("NULL");
    }


    void main(){

	    int n;
	    printf("\n Enter the no. of nodes u want  :   ");
	    scanf("%d",&n);

	    for (int i=1;i<=n;i++){

		    addnode();

	    }

	    printdata();

	    printf("\n\n");

    }
