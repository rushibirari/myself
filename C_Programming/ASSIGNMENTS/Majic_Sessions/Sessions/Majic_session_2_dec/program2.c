//
//
//WAP to create the node given by user and get data and print all nodes..
//information of states in maharashtra..

//
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct States{

	char s_name[20];
	int s_population;
	float s_budget;
	float s_lit_rate;

	struct States *next;

}state;

  state *head=NULL;// global declaration..

  void addnode(){

	  state *newnode=(state *)malloc(sizeof(state));

	  getchar();
	  printf("Enter the state Name          : ");
	  fgets(newnode->s_name,20,stdin);

	  printf("Enter the state population    :  ");
	  scanf("%d",&newnode->s_population);

	  printf("Enter the state budget        : ");
	  scanf("%f",&newnode->s_budget);

	  printf("Enter the state literacy rate :");
	  scanf("%f",&newnode->s_lit_rate);

	  newnode->next=NULL;

	  //getchar();
	
	  if(head==NULL){
		  head=newnode;
	  }
	  else{
		  state *temp=head;

		  while(temp->next!= NULL){

			  temp=temp->next;
			 }
		  temp->next=newnode;

	  }

       }


  void printdata(){

	  state *temp=head;

	  printf("\n The linked list is  :  \n");
	  while(temp!=NULL){
		  temp->s_name[strcspn(temp->s_name,"\n")]='\0';
		 

		  printf("| %s =  %d  =  %.4f  = %.2f  |-->",temp->s_name,temp->s_population,temp->s_budget,temp->s_lit_rate);

		  temp=temp->next;

	  }
	  printf("NULL");

	 }


     void main(){

	     int n ;

	     printf("Enter the number of nodes u want add :  ");
	     scanf("%d",&n);

	     for(int i=1;i<=n;i++){
		     addnode();
	     }

	     printdata();

	     printf("\n\n");

     }
