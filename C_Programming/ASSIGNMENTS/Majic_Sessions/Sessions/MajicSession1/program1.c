//WAP to print the table of input number using pointer
//
#include<stdio.h>
void main(){
	
	int num;
	int arr[10];
	printf("Enter the number : \n\n");
	scanf("%d",&num);
	//int *ptr=&num;
	//
	for(int i=0;i<10;i++){
		arr[i]=num*(i+1);
		//printf("%d   ",(*ptr * i));
	}
	for(int i=0;i<10;i++){
		printf("%d    ",*(arr+i));
	}
	printf("\n\n");

}
