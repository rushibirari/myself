
#include<stdio.h>

int *ptr=NULL;


void fun(){
	int x=10;
	ptr=&x;
	printf("%d\n",x);//10
	printf("%d\n",*ptr);//10

}
void main(){
  	int x=20;
	printf("%d\n",x);//20
	fun();
	printf("%d\n",*ptr);//20.. It stores value of x because of dangling pointer

}
