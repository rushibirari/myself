
/*  WAP to check whether the input is a leap year or not
 *  */

 #include<stdio.h>
 void main()
{
  int i, year;
  printf("\n  Enter the year which you want to check:");
  scanf("%d",&year);

  if( (year%4==0) && (year%100==0))
      { 
         if(year%400==0)
         {
           printf("\n %d is a leap year..\n\n",year);
         }
      }
  else
  {
    printf("\n %d is not a leap year..\n\n",year);
  }
}
